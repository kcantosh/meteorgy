MODULE algebra3d
 USE params , ONLY: dp
 IMPLICIT NONE
 REAL(dp),PARAMETER   ::  pi = 3.1415926535897932384626433D0
! ----------------------------------------------------------------------------------------
! Plain 3-dimensional linear algebra stuff
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------


INTERFACE
  SUBROUTINE dsyevj3s(A,W)
  USE params , ONLY: dp
    ! modified to skip some loops used for eigenvectors
    REAL(dp) , INTENT(INOUT) :: A(3,3)   ! symmetric input matrix
    REAL(dp) , INTENT(OUT)   :: W(3)     ! eigenvalues
  END SUBROUTINE dsyevj3s
END INTERFACE

INTERFACE
  SUBROUTINE dsyevj3(A,Q,W)
  USE params , ONLY: dp
    REAL(dp) , INTENT(INOUT) :: A(3,3)   ! symmetric input matrix
    REAL(dp) , INTENT(OUT)   :: Q(3)     ! eigenvectors
    REAL(dp) , INTENT(OUT)   :: W(3)     ! eigenvalues
  END SUBROUTINE dsyevj3
END INTERFACE

CONTAINS

  PURE FUNCTION value(v) RESULT(r)
    ! value of vector
    REAL(dp) , INTENT(IN)  :: v(3)
    REAL(dp) :: r

    r = SQRT( v(1)*v(1) + v(2)*v(2) + v(3)*v(3) )
  END FUNCTION value

  PURE FUNCTION dot3d(v,w) RESULT(r)
    REAL(dp)  , INTENT(IN)    :: v(3) , w(3)
    REAL(dp)                  :: r

    r = v(1) * w(1) + v(2) * w(2) + v(3) * w(3)
  END FUNCTION dot3d

  PURE FUNCTION cross3d(v,w) RESULT(r)
    REAL(dp)  , INTENT(IN)    :: v(3) , w(3)
    REAL(dp)                  :: r(3)

    r(1) = v(2) * w(3) - v(3) * w(2)
    r(2) = v(3) * w(1) - v(1) * w(3)
    r(3) = v(1) * w(2) - v(2) * w(1)
  END FUNCTION cross3d

  PURE FUNCTION ncross3d(v,w) RESULT(r)
    REAL(dp)  , INTENT(IN)    :: v(3) , w(3)
    REAL(dp)                  :: r(3)

    r = cross3d(v,w)
    r = r/value(r)
  END FUNCTION ncross3d

  PURE FUNCTION trans3d(A,v) RESULT(r)
    ! Vector transformation V*T
    REAL(dp)  , INTENT(IN)    :: A(3,3) , v(3)
    REAL(dp)                  :: r(3)

    r(1) = A(1,1)*v(1) + A(1,2)*v(2) + A(1,3)*v(3)
    r(2) = A(2,1)*v(1) + A(2,2)*v(2) + A(2,3)*v(3)
    r(3) = A(3,1)*v(1) + A(3,2)*v(2) + A(3,3)*v(3)
  END FUNCTION trans3d

  PURE FUNCTION det3d(A) RESULT(r)
    REAL(dp) , INTENT(IN)    :: A(3,3)
    REAL(dp)                 :: r
  
    r = A(1,1) * A(2,2) * A(3,3) &
      - A(1,1) * A(2,3) * A(3,2) &
      + A(1,2) * A(2,3) * A(3,1) &
      - A(1,2) * A(2,1) * A(3,3) &
      + A(1,3) * A(2,1) * A(3,2) &
      - A(1,3) * A(2,2) * A(3,1)
  END FUNCTION det3d
  
  PURE FUNCTION matinv3d(A) RESULT(R)
    ! Performs a direct calculation of the inverse of a 3x3 matrix.
    REAL(dp) , INTENT(IN)    :: A(3,3)       ! Matrix
    REAL(dp)                 :: R(3,3)       ! Inverse matrix
    REAL(dp)                 :: rd
  
    ! Calculate the reciprocal determinant
    rd = 1.0D0/det3d(A)
  
    ! Calculate the inverse of the matrix
    R(1,1) = +rd * ( A(2,2) * A(3,3) - A(2,3) * A(3,2) )
    R(2,1) = -rd * ( A(2,1) * A(3,3) - A(2,3) * A(3,1) )
    R(3,1) = +rd * ( A(2,1) * A(3,2) - A(2,2) * A(3,1) )
    R(1,2) = -rd * ( A(1,2) * A(3,3) - A(1,3) * A(3,2) )
    R(2,2) = +rd * ( A(1,1) * A(3,3) - A(1,3) * A(3,1) )
    R(3,2) = -rd * ( A(1,1) * A(3,2) - A(1,2) * A(3,1) )
    R(1,3) = +rd * ( A(1,2) * A(2,3) - A(1,3) * A(2,2) )
    R(2,3) = -rd * ( A(1,1) * A(2,3) - A(1,3) * A(2,1) )
    R(3,3) = +rd * ( A(1,1) * A(2,2) - A(1,2) * A(2,1) )
  END FUNCTION matinv3d

  PURE FUNCTION angle3d(u,v) RESULT(r)
    ! angle between u and v
    REAL(dp) , INTENT(IN)    :: u(3) , v(3)
    REAL(dp) :: r

    r = ACOS( dot3d(u,v) /( value(u) * value(v) ) )
  END FUNCTION angle3d

  PURE FUNCTION dihedral3d(u,v,w) RESULT(r)
    ! dihedral angle of u and w along axis v
    ! https://math.stackexchange.com/questions/47059/how-do-i-calculate-a-dihedral-angle-given-cartesian-coordinates
    REAL(dp) , INTENT(IN)    :: u(3) , v(3) , w(3)
    REAL(dp) :: n(3) , m(3) , o(3)
    REAL(dp) :: x , y , r

    ! make orthonormal frame aligned with v and (v,w)
    n = ncross3d(v,w)
    o = ncross3d(n,v)
    
    ! orthonormal vector on (v,u)
    m = ncross3d(v,u)

    x = dot3d(n,m)
    y = dot3d(o,m) 

    r = ATAN2(y,x)
  END FUNCTION dihedral3d

  PURE FUNCTION rotmat3d(u,p) RESULT(R)
    ! see Wikipedia for more information:
    ! https://en.wikipedia.org/w/index.php?title=Rotation_matrix&oldid=875545324#Rotation_matrix_from_axis_and_angle
    REAL(dp)  , INTENT(IN)    :: u(3), p   ! NOTE that u has to be a unit vector
    REAL(dp)                  :: R(3,3)

    ! Calculate sine and cosine terms
    REAL(dp) :: sn, cs, cs_1
    sn=SIN(P);  cs= COS(P); cs_1=1.0D0-cs

    R(1,1) = u(1)*u(1) * cs_1 +        cs
    R(2,1) = u(2)*u(1) * cs_1 + u(3) * sn
    R(3,1) = u(3)*u(1) * cs_1 - u(2) * sn
    R(1,2) = u(1)*u(2) * cs_1 - u(3) * sn
    R(2,2) = u(2)*u(2) * cs_1 +        cs
    R(3,2) = u(3)*u(2) * cs_1 + u(1) * sn
    R(1,3) = u(1)*u(3) * cs_1 + u(2) * sn
    R(2,3) = u(2)*u(3) * cs_1 - u(1) * sn
    R(3,3) = u(3)*u(3) * cs_1 +        cs
  END FUNCTION rotmat3d

  PURE FUNCTION rot_align(v,w) RESULT(R)
    ! create rotation matrix that aligns v with w
    REAL(dp) , INTENT(IN)    :: v(3) , w(3)
    REAL(dp)  :: R(3,3)

    R = rotmat3d( ncross3d(v,w) , angle3d(v,w) )
  END FUNCTION rot_align

  PURE FUNCTION rot_align_plane(u,v,w) RESULT(R)
    ! create rotation matrix that rotates around v until u is aligned with the 
    ! plane spanned by v and w. u points to the same side as w relative to v
    REAL(dp) , INTENT(IN)    :: u(3) , v(3) , w(3)
    REAL(dp)  :: p , R(3,3)

    p = dihedral3d(u,v,w)
    R = rotmat3d( v/value(v) , p )
  END FUNCTION rot_align_plane

  ! ----------------------------------------------------------------------------
  ! more elaborate/specific
  ! ----------------------------------------------------------------------------
  PURE FUNCTION boxheight( box , plane ) RESULT( height )
    ! height of box lying on plane
    REAL(dp)  , INTENT(IN)  :: box(3,3)
    INTEGER   , INTENT(IN)  :: plane
    INTEGER   :: x,y
    REAL(dp)  :: height
    
    x  = MODULO(plane,3)+1
    y  = MODULO(plane+1,3)+1
    height  = det3d(box) / value(cross3d( box(:,x) , box(:,y) ))
  END FUNCTION boxheight

  PURE SUBROUTINE default_align_box( box )
    ! align unit cell saved in matrix box(3,3) with a=box(:,1) such that
    ! a=x, b=(x,y)
    REAL(dp)  , INTENT(INOUT)  :: box(3,3)
    REAL(dp) :: x(3) , y(3)
    x = (/ 1.0D0 , 0.0D0 , 0.0D0 /)
    y = (/ 0.0D0 , 1.0D0 , 0.0D0 /)

    ! a = |a| x
    box = MATMUL(  rot_align(box(:,1) , x) , box )
    
    ! b E (x,y)
    box = MATMUL(  rot_align_plane(box(:,2) , x , y )  ,  box  )
  END SUBROUTINE default_align_box

END MODULE algebra3d
