MODULE params
! ----------------------------------------------------------------------------------------
! Global parameters, might be changed for porting
! Copyright (C) 2018 Adrian Hühn
! ----------------------------------------------------------------------------------------
  CHARACTER(LEN=*) , PARAMETER  :: comment_ = "written with meteorgy"
  CHARACTER(LEN=*) , PARAMETER  :: devnull  = "/dev/null"
  CHARACTER(LEN=*) , PARAMETER  :: randsrc  = "/dev/urandom"

  INTEGER , PARAMETER  ::    sp     = selected_real_kind(  6 ,   37 )
  INTEGER , PARAMETER  ::    dp     = selected_real_kind( 15 ,  307 )
  INTEGER , PARAMETER  ::    qp     = selected_real_kind( 33 , 4931 )

  INTEGER , PARAMETER  ::  sstr_    =   8   , & ! short string
                            str_    =  80   , & ! normal string
                           lstr_    = 300       ! long string

  REAL(dp),PARAMETER   ::  pi_      = 3.1415926535897932384626433D0

  INTEGER , PARAMETER  ::  stdin_   =   5   , & ! standard input
                           stdout_  =   6   , & ! standard output
                           stderr_  =   0   , & ! standard error output
                           inunit_  =  42   , & ! input file unit
                           outunit_ =  43       ! input file unit

  INTEGER              ::  stdin   = stdin_   &
                        ,  stdout  = stdout_  &
                        ,  stderr  = stderr_  &
                        ,  inunit  = inunit_  &
                        ,  outunit = outunit_


 CONTAINS

  SUBROUTINE out2err()
    stdout = stderr
  END SUBROUTINE out2err

  SUBROUTINE noerr()
    OPEN(unit=44,file=devnull,status="REPLACE")
    stderr = 44
  END SUBROUTINE noerr


  SUBROUTINE reset_streams()
    stdin   = stdin_
    stdout  = stdout_
    stderr  = stderr_
    inunit  = inunit_
    outunit = outunit_
  END SUBROUTINE reset_streams


  SUBROUTINE rnginit()
    ! non-portable way to initialize random number generator on Linux
    INTEGER , ALLOCATABLE :: seed(:)
    INTEGER :: i, n, un, istat, dt(8), pid
  
    CALL RANDOM_SEED(size = n)
    ALLOCATE(seed(n))

    OPEN(unit=45, file="/dev/urandom", access="stream", &
         form="unformatted", action="read", status="old", iostat=istat)
    READ(45) seed
    CLOSE(45)

    CALL RANDOM_SEED(put = seed)
    DEALLOCATE( seed )

  END SUBROUTINE rnginit

END MODULE params
