PROGRAM rewrite
! ----------------------------------------------------------------------------------------
! Plain 3-dimensional linear algebra stuff
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
  USE params  , ONLY: lstr_
  USE types   , ONLY: pbc , pbc_print , atom_print , pbc_convert
  USE file_io , ONLY: vasp_read , vasp_write
  ! debug
  USE accounting
  IMPLICIT NONE
  ! filenames
  CHARACTER(LEN=:) , ALLOCATABLE :: infile , query

  ! cell data
  TYPE(pbc) :: geom
  ! control
  INTEGER   :: stat , i

  ! accounting debug
  INTEGER , ALLOCATABLE :: mask(:)

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile , query )

  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Usage:  rewrite <infile> [<cartesian/direct/verbose>]'
    STOP
  ENDIF
  infile = TRIM(infile)
  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=query  , STATUS=stat )
  IF( stat /= 0 ) THEN
    query = '-'
  ENDIF

  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  CALL vasp_read( infile , geom , stat )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! DO STUFF BELOW
  ! ----------------------------------------------------------------------------
  CALL pbc_convert( geom , 0 , stat)
  CALL atom_print( geom%atm , .FALSE. , .FALSE. )
  CALL pbc_convert( geom , 2 , stat)
  CALL atom_print( geom%atm , .FALSE. , .FALSE. )
  CALL pbc_convert( geom , 5 , stat)
  CALL atom_print( geom%atm , .FALSE. , .FALSE. )
  CALL pbc_convert( geom , 8 , stat)
  CALL atom_print( geom%atm , .FALSE. , .FALSE. )
  CALL pbc_convert( geom , 3 , stat)
  CALL atom_print( geom%atm , .FALSE. , .FALSE. )
  
  CALL sort_one( 'e+' , geom%atm )
  !CALL sort_in_direction( geom%atm , 3 )

  IF( query == '-' ) THEN
    ! do nothing
  ELSEIF( query(1:1) == 'v' ) THEN
    CALL pbc_print( geom )
  ELSEIF( query(1:1) == 'c' ) THEN
    CALL pbc_convert( geom , 0 , stat )
    IF( stat /= 0 ) THEN
      WRITE(*,*) 'Unknown system.'
      STOP 255
    ENDIF
  ELSEIF( query(1:1) == 'd' ) THEN
    CALL pbc_convert( geom , 3 , stat )
    IF( stat /= 0 ) THEN
      WRITE(*,*) 'Unknown system.'
      STOP 255
    ENDIF
  ENDIF
  ! ----------------------------------------------------------------------------
  ! WRITE OUTPUT
  ! ----------------------------------------------------------------------------

  ! write POSCAR
  CALL vasp_write( infile//'.out' , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM rewrite
