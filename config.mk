# meteorgy version
VERSION = 0.0.2

# Customize below to fit your system

# path
BIN     = ~/.local/bin

# flags
FC      = gfortran
FFLAGS  = -O2
#FFLAGS  = -O2 -fdefault-real-8 -fdefault-double-8
STD     = f2003