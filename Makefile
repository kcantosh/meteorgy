
include config.mk

# rules to compile a .o from each .f and .f90
%.o: %.f
	@echo "make $@ from $<"
	$(FC) -ffixed-form $(FFLAGS) -c $< -o $@
%.o: %.f90
	@echo "make $@ from $<"
	$(FC) -std=$(STD) -ffree-form $(FFLAGS) -c $< -o $@


# executables
EXE = rewrite vaspinfo vasp2xyz xyz2vasp vasp2xsf slabcut

# libraries
FLIB = params.o dsyevj3.o algebra3d.o parse.o types.o accounting.o file_io.o meteorgy.o

# 
all: $(EXE)


install: all
	cp $(EXE) $(BIN)

clean:
	rm -f *.o *.mod $(EXE)


# build executables
rewrite: $(FLIB) rewrite.o
	$(FC) -std=$(STD) -o $@ $^

vaspinfo: $(FLIB) vaspinfo.o
	$(FC) -std=$(STD) -o $@ $^

vasp2xyz: $(FLIB) vasp2xyz.o
	$(FC) -std=$(STD) -o $@ $^

xyz2vasp: $(FLIB) xyz2vasp.o
	$(FC) -std=$(STD) -o $@ $^

vasp2xsf: $(FLIB) vasp2xsf.o
	$(FC) -std=$(STD) -o $@ $^

slabcut: $(FLIB) slabcut.o
	$(FC) -std=$(STD) -o $@ $^
