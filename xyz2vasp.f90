PROGRAM run
! ----------------------------------------------------------------------------------------
! Plain 3-dimensional linear algebra stuff
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
  USE params  , ONLY: dp , lstr_
  USE types   , ONLY: pbc , pbc_convert , makebox , pbc_print
  USE file_io , ONLY: xyz_read , vasp_write , random_filename , file , path
  USE accounting , ONLY: sort_one
  USE meteorgy, ONLY: fix_molecule , shift_atoms , center_xyz
  IMPLICIT NONE
  ! filenames
  CHARACTER(LEN=:) , ALLOCATABLE :: infile , query
  CHARACTER(LEN=lstr_) :: outfile
  REAL(dp)  :: dist , shift(3)

  ! cell data
  TYPE(pbc) :: geom
  ! control
  INTEGER   :: l , stat

  ALLOCATE( CHARACTER(LEN=lstr_) :: infile , query )

  ! ----------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! ----------------------------------------------------------------------------

  CALL GET_COMMAND_ARGUMENT( 1 , VALUE=infile , STATUS=stat )
  IF( stat /= 0 ) THEN
    WRITE(*,*) 'Usage:  vasp2xyz <infile> [<distance>]'
    WRITE(*,*) '    - Distance from periodic images, default: 20 Ansgtrom.'
    STOP
  ENDIF
  infile = TRIM(infile)

  CALL GET_COMMAND_ARGUMENT( 2 , VALUE=query  , STATUS=stat )
  IF( stat /= 0 ) THEN
    dist = 20.0
  ELSE
    READ(query,*) dist
  ENDIF

  ! ----------------------------------------------------------------------------
  ! READ INPUT
  ! ----------------------------------------------------------------------------

  ! read POSCAR
  CALL xyz_read( infile , geom%atm , stat , comment=geom%comment )
  IF( stat /= 0 ) STOP 1

  ! ----------------------------------------------------------------------------
  ! create box, move system to middle of the box
  ! ----------------------------------------------------------------------------

  geom%sys = 0
  geom%nat = SIZE(geom%atm)
  CALL makebox( geom%box , .TRUE. , dist )

  shift = ( geom%box(:,1)+geom%box(:,2)+geom%box(:,3) )/2.0D0 - center_xyz(geom%atm)

  CALL shift_atoms( shift , geom%atm )

  CALL sort_one( 'e+' , geom%atm )

  ! ----------------------------------------------------------------------------
  ! WRITE OUTPUT (derive POSCAR.* from *.xyz)
  ! ----------------------------------------------------------------------------
  l = LEN(infile)
  IF( infile == '-' ) THEN
    outfile = 'POSCAR.'//random_filename( 3 )
  ELSEIF( infile(l-3:l) == '.xyz' ) THEN
    outfile = path(infile)//'POSCAR.'//file(infile(1:l-4))
  ELSE
    outfile = path(infile)//'POSCAR.'//file(infile)
  ENDIF

  ! write POSCAR
  !CALL pbc_print( geom )
  CALL vasp_write( outfile , geom , stat )

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM run
