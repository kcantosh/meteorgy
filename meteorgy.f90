MODULE meteorgy
USE params
USE algebra3d  , ONLY: dot3d , boxheight
USE types      , ONLY: pbc , atom , element_weight , pbc_convert , mix_box , get_sys ! , atom_print , box_print
!USE types      , ONLY: pbc , atom
USE accounting , ONLY: sort_one
! ----------------------------------------------------------------------------------------
! Manipulate geometry datasets
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
  IMPLICIT NONE

 CONTAINS

  SUBROUTINE make_slab( geom , plane , upside , downside , vacuum )
    ! --------------------------------------------------------------------------
    ! make slab along two unit cell directions
    !
    ! depending on whether vacuum is included
    ! - multiply cell along unit cell depending on range to fit additional atoms
    ! - make vacuum vector perpendicular to periodic lattice vectors
    ! --------------------------------------------------------------------------
    TYPE(pbc)  , INTENT(INOUT)  :: geom
    INTEGER    , INTENT(IN)     :: plane
    REAL(dp)   , INTENT(IN)     :: upside , downside
    REAL(dp)   , INTENT(IN)     &
               , OPTIONAL       :: vacuum

    TYPE(atom)       &
      ,  ALLOCATABLE :: atm(:)
    CHARACTER(LEN=2) :: sortkey
    REAL(dp) , PARAMETER  :: minthickness = 4.0D0 , defaultseparation = 10.0D0
    REAL(dp)         :: height , up , down , tmp, thickness , vac
    INTEGER          :: p , fa , la , fb , lb
    INTEGER          :: i , j , k , m , n
    INTEGER          :: stat , nat

    ! --------------------------------------------------------------------------
    ! get all auxiliary values, make work copy in atm
    ! --------------------------------------------------------------------------

    ! plane of choice, bulk nat
    nat = geom%nat
    p = MODULO(plane-1,3)+1

    ! geom in fractional units
    height = boxheight( geom%box , p )
    CALL pbc_convert( geom , 3 , stat )
    
    ! boundaries ordered and in fractional units
    up   = MERGE( upside , downside , upside >= downside ) / height
    down = MERGE( upside , downside , upside <  downside ) / height

    ! make wrapped and sorted copy of atoms
    ALLOCATE( atm(nat) )
    DO i = 1,nat
      atm(i)      = geom%atm(i)
      atm(i)%r(p) = geom%atm(i)%r(p) - FLOOR( geom%atm(i)%r(p) )
    ENDDO
    WRITE(sortkey,'(I1,A1)') p , '-'
    CALL sort_one( sortkey , atm )

    
    ! get first atom and block
    fb = FLOOR( up )
    tmp = up - REAL(fb,dp)
    fa = 1
    DO WHILE( atm(fa)%r(p) >= tmp )
      fa = fa+1
    ENDDO
    
    ! get last atom and block
    lb  = FLOOR( down )
    tmp = down - REAL(lb,dp)
    la = nat
    DO WHILE( atm(la)%r(p) <= tmp )
      la = la-1
    ENDDO

    
    ! --------------------------------------------------------------------------
    ! make slab
    ! --------------------------------------------------------------------------

    ! reallocate geom
    DEALLOCATE( geom%atm )
    geom%nat = nat*(fb-lb)-fa+la+1
    ALLOCATE( geom%atm(geom%nat) )

!    WRITE(*,*) 'DEBUGINFO, upside, downside, up , down , height:', upside, downside, up, down, height
!    WRITE(*,*) 'DEBUGINFO, nat, geom%nat, fb, lb, fa, la', nat, geom%nat, fb, lb, fa, la

    m = fa
    n = nat
    k = 1
    DO i = fb,lb,-1
      IF( i == lb ) n = la
      DO j = m,n
        geom%atm(k)      = atm(j)
        geom%atm(k)%r(p) = atm(j)%r(p) + REAL(i,dp)
        k = k + 1
      ENDDO
      m = 1
    ENDDO
    
    ! get thickness
    thickness = height * ( geom%atm(1)%r(p) - geom%atm(geom%nat)%r(p) )

    ! convert to cartesian (might change in future)
    CALL pbc_convert( geom , 0 , stat )
    
    ! --------------------------------------------------------------------------------------
    ! Calculate cell vector independent from slab
    ! --------------------------------------------------------------------------------------
    IF( PRESENT(vacuum) ) THEN
      ! shift slab to center of mass and make cell vector perpendicular to slab and value of
      ! 'vacuum'

      ! shift to zero
      CALL shift_atoms( -center_slab(geom,p) , geom%atm )

      ! set a sane vacuum (set a default for low vacuum values)
      vac = MERGE( vacuum , thickness + defaultseparation , vacuum > thickness + minthickness )

      ! get perpendicular unit cell vector
      geom%box = mix_box( geom%box , get_sys(plane=p) )
      geom%box(:,p) = geom%box(:,p) * vac

    ELSE
      ! duplicate unit cell to fit thickness
      tmp = REAL((fb-lb+1),dp)
      geom%box(:,p) = geom%box(:,p) * tmp
    ENDIF

  END SUBROUTINE make_slab


  PURE SUBROUTINE shift_atoms( shift , atm )
    ! --------------------------------------------------------------------------
    ! get center of mass of aperiodic system
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    REAL(dp)   , INTENT(IN)    :: shift(3)
    INTEGER   :: i

    DO i=1,SIZE(atm)
      atm(i)%r = atm(i)%r + shift
    ENDDO
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE shift_atoms


  PURE FUNCTION center_xyz( atm ) RESULT( center )
    ! --------------------------------------------------------------------------
    ! get center of mass of aperiodic system
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(IN) :: atm(:)
    REAL(dp)                :: center(3)
    REAL(dp)  :: mxyz(3) , m , mm
    INTEGER   :: i

    mxyz(:) = 0.0D0
    mm      = 0.0D0

    DO i=1,SIZE(atm)
      m       = element_weight(atm(i)%e) 
      mxyz(:) = mxyz(:) + m * atm(i)%r
      mm      = mm      + m
    ENDDO

    center = mxyz(:) / mm
    ! ---------------------------------------------------------------------- END
  END FUNCTION center_xyz


  FUNCTION center_slab( geom , plane ) RESULT( center )
    ! --------------------------------------------------------------------------
    ! get center of mass for slabs and wires. shift perpendicular to periodic
    ! direction(s)
    !
    ! - convert to desired system
    ! - in the general case use fix_slab before centering
    ! - subroutine converts coordinates by key
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(IN)  :: geom
    INTEGER   , INTENT(IN)  :: plane
    TYPE(pbc) :: g
    REAL(dp)  :: center(3) , dir(3) , proj
    INTEGER   :: p , stat

    p = MODULO(plane-1,3)+1

    ! get center
    IF( geom%sys /= 0 ) THEN
      g = geom
      CALL pbc_convert( g , 0 , stat )
      center =  center_xyz( g%atm )
      DEALLOCATE( g%atm )
    ELSE
      center =  center_xyz( geom%atm )
    END IF

    ! remove component in periodic directions
    dir = geom%box(:,p)
    proj = dot3d( dir , center ) / ( dir(1)**2 + dir(2)**2 + dir(3)**2 )
    center = dir * proj
    ! ---------------------------------------------------------------------- END
  END FUNCTION center_slab
  

  SUBROUTINE fix_slab( geom , plane , maxdist )
    ! --------------------------------------------------------------------------
    ! repair a disjoint multislab that is split along unit cell borders
    !
    ! What it does:  reorder (!), find vacuum layer, wrap atoms to joint layer
    !
    ! What it does not:  shift atoms within unit cell (all atoms are between
    !                    -dir and +dir
    ! --------------------------------------------------------------------------
    TYPE(pbc)        ,  INTENT(INOUT)  :: geom
    INTEGER            ,   INTENT(IN)  :: plane
    REAL(dp) , OPTIONAL , INTENT(OUT)  :: maxdist

    CHARACTER(LEN=2) :: sortkey
    REAL(dp)         :: gap , maxgap , cutpos
    INTEGER          :: stat
    INTEGER          :: i , p

    p = MODULO(plane-1,3)+1
    WRITE(sortkey,'(I1,A1)') p , '-'

    CALL pbc_convert( geom , 3 , stat )

    ! wrap into cell
    DO i = 1,geom%nat
      geom%atm(i)%r(p) = geom%atm(i)%r(p) - FLOOR( geom%atm(i)%r(p) )
    ENDDO

    ! sort in chosen direction
    CALL sort_one( sortkey , geom%atm )

    ! find cut position
    maxgap = geom%atm(geom%nat)%r(p) - geom%atm(1)%r(p) + 1.0D0
    cutpos = geom%atm(1)%r(p) + 1.0D0 - maxgap/2.0D0
    DO i = 2,geom%nat
      gap = geom%atm(i-1)%r(p) - geom%atm(i)%r(p)
      IF( gap > maxgap ) THEN
        maxgap = gap
        cutpos = geom%atm(i)%r(p) + maxgap/2.0
      END IF
    ENDDO

    ! cut, wrap atoms below cut, connecting slab
    DO i = 1,geom%nat
      IF( geom%atm(i)%r(p) > cutpos ) THEN
        geom%atm(i)%r(p) = geom%atm(i)%r(p) - 1.0D0
      END IF
    ENDDO

    ! return vacuum distance
    IF( PRESENT(maxdist) ) maxdist = maxgap
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE fix_slab


  SUBROUTINE fix_wire( geom , axis )
    ! --------------------------------------------------------------------------
    ! apply fix_slab subroutine two times... having cake
    ! --------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT)  :: geom
    INTEGER   , INTENT(IN)     :: axis
    CALL fix_slab( geom , axis+1 )
    CALL fix_slab( geom , axis+2 )   ! (fixed by modulo in fix_slab)
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE fix_wire


  SUBROUTINE fix_molecule( geom )
    ! --------------------------------------------------------------------------
    ! apply fix_slab subroutine three times... having a cookie.
    ! --------------------------------------------------------------------------
    TYPE(pbc)     , INTENT(INOUT)  :: geom
    CALL fix_slab( geom , 1 )
    CALL fix_slab( geom , 2 )
    CALL fix_slab( geom , 3 )
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE fix_molecule


  SUBROUTINE standardize_box( geom , stat )
    USE algebra3d, ONLY: default_align_box
    ! --------------------------------------------------------------------------
    ! rigorously standardize given geometry (W.I.P)
    ! useful starting point to compare different calculations
    ! --------------------------------------------------------------------------
    REAL(dp)  , PARAMETER      :: thr = 1.0D-4
    TYPE(pbc) , INTENT(INOUT)  :: geom
    INTEGER   , INTENT(OUT)    :: stat

    ! check and modify unit cell. find angle closest to 90 deg

    ! find Bravais lattice and swap vectors according to conventions. (this is
    ! also possible for surfaces, there are 5 2D Bravais lattices)
    
    ! rotate to standard direction
    CALL pbc_convert( geom , 3 , stat )
    CALL default_align_box( geom%box )

    ! round small components to zero
    geom%box(1,1) = MERGE( 0.0D0 , geom%box(1,1) , ABS(geom%box(1,1)) < thr )
    geom%box(2,1) = MERGE( 0.0D0 , geom%box(2,1) , ABS(geom%box(2,1)) < thr )
    geom%box(3,1) = MERGE( 0.0D0 , geom%box(3,1) , ABS(geom%box(3,1)) < thr )
    geom%box(1,2) = MERGE( 0.0D0 , geom%box(1,2) , ABS(geom%box(1,2)) < thr )
    geom%box(2,2) = MERGE( 0.0D0 , geom%box(2,2) , ABS(geom%box(2,2)) < thr )
    geom%box(3,2) = MERGE( 0.0D0 , geom%box(3,2) , ABS(geom%box(3,2)) < thr )
    geom%box(1,3) = MERGE( 0.0D0 , geom%box(1,3) , ABS(geom%box(1,3)) < thr )
    geom%box(2,3) = MERGE( 0.0D0 , geom%box(2,3) , ABS(geom%box(2,3)) < thr )
    geom%box(3,3) = MERGE( 0.0D0 , geom%box(3,3) , ABS(geom%box(3,3)) < thr )


  END SUBROUTINE standardize_box


  SUBROUTINE split_bond( atma , atmb , stat )
    ! --------------------------------------------------------------------------
    ! make initial search vector saved in velocities,
    ! useful starting point in dimer TS searches
    ! CAUTION: cartesian are assumed!
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(INOUT)  :: atma , atmb
    INTEGER    , INTENT(OUT)    :: stat
    REAL(dp)  :: dist , diff(3) , ma , mb

    ! check correctness of input
    stat = 0
    diff = atma%r - atmb%r
    dist = SQRT( SUM( diff**2 ) )

    IF( dist <= 0.5 ) THEN
      stat = 1
      RETURN
    ENDIF

    ! make normalized vector
    diff = diff / dist

    ! get inverse sqrt masses
    ma = 1 / SQRT( element_weight( atma%e ) )
    mb = 1 / SQRT( element_weight( atmb%e ) )

    ! set up velocity
    atma%v = -ma / (ma+mb) * diff
    atmb%v = +mb / (ma+mb) * diff
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE split_bond


END MODULE meteorgy
