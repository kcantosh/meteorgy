PROGRAM run
! ----------------------------------------------------------------------------------------
! Make initial search vector based on one bond for improved dimer search in VASP
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
  USE params,    ONLY: dp , stdout
  USE file_io,   ONLY: vasp_read , vasp_write
  USE types,     ONLY: pbc , atom , get_sys , mix_box , box_print , atom_print , pbc_convert
  USE meteorgy,  ONLY: make_slab , center_slab , shift_atoms , standardize_box
  USE accounting, ONLY: sort_one
  USE parse,     ONLY: keyplane
  IMPLICIT NONE
  ! filenames
  CHARACTER(LEN=256)    ::  infile  ="POSCAR"

  ! cell data
  TYPE(pbc)             :: geom , newgeom
  TYPE(atom)            :: tmpatoms
  INTEGER , ALLOCATABLE :: selection(:)

  ! new cell 
  REAL(dp)              :: height , scale , area
  INTEGER               :: most

  ! chosen atoms
  CHARACTER(LEN=2)      :: key = 'xy'
  REAL(dp)              :: vacuum = 0.0D0 , thickness = 0.0D0 , shift = 0.0D0

  ! control
  CHARACTER(LEN=80)     :: arg
  INTEGER               :: nargs , n , stat
  INTEGER               :: p
  LOGICAL               :: failure = .FALSE.


  ! --------------------------------------------------------------------------------------
  ! PARSE COMMAND LINE
  ! --------------------------------------------------------------------------------------
  nargs = COMMAND_ARGUMENT_COUNT()
  IF( nargs == 5 ) THEN

    CALL GET_COMMAND_ARGUMENT( 1 , value=arg , status=stat )
    READ(arg,*,iostat=stat) key
    IF( stat /= 0 ) THEN
      failure = .TRUE.
      WRITE(*,'(A)') "Could not read plane key! Use something like 'ab'."
    ENDIF

    CALL GET_COMMAND_ARGUMENT( 2 , value=arg , status=stat )
    READ(arg,*,iostat=stat) thickness
    IF( stat /= 0 ) THEN
      failure = .TRUE.
      WRITE(*,'(A)') 'Could not read thickness!'
    ENDIF

    CALL GET_COMMAND_ARGUMENT( 3 , value=arg , status=stat )
    READ(arg,*,iostat=stat) shift
    IF( stat /= 0 ) THEN
      failure = .TRUE.
      WRITE(*,'(A)') 'Could not read shift, set to zero'
    ENDIF

    CALL GET_COMMAND_ARGUMENT( 4 , value=arg , status=stat )
    READ(arg,*,iostat=stat) vacuum
    IF( stat /= 0 ) THEN
      failure = .TRUE.
      WRITE(*,'(A)') 'Could not read vacuum distance!'
    ENDIF

    CALL GET_COMMAND_ARGUMENT( 5 , value=infile , status=stat )
    IF( stat /= 0 ) THEN
      failure = .TRUE.
      WRITE(*,'(A)') 'Something really weird must have happened! Cannot read filename.'
    ENDIF

  ELSE

    failure = .TRUE.
    WRITE(*,'(A)') 'Incomplete arguments...'
    
  ENDIF

  ! --------------------------------------------------------------------------------------
  ! READ INPUT
  ! --------------------------------------------------------------------------------------

  ! read POSCAR
  CALL vasp_read( infile , geom , stat )
  IF( stat /= 0 ) THEN
    failure = .TRUE.
  ENDIF

!u4vvhbegdcex

  ! stop if error
  IF( failure ) THEN
    WRITE(*,'(A)') "Usage:  slabcut <plane> <thickness> <shift> <vacuum> <input file>"
    STOP
  ENDIF

  ! make slab
  p = keyplane(key)
  CALL make_slab( geom , p , thickness+shift , shift , vacuum )
  
  ! shift to center
  CALL pbc_convert( geom , 0 , stat )
  CALL shift_atoms( geom%box(:,p)*0.5D0, geom%atm )

  ! rotate cell
  CALL standardize_box( geom , stat )

  CALL sort_one( 'e+' , geom%atm )

  WRITE(stdout,'()')
  WRITE(stdout,'(A)') 'New unit cell:'
  CALL box_print( geom%box )
  WRITE(stdout,'()')

  ! --------------------------------------------------------------------------------------
  ! WRITE OUTPUT
  ! --------------------------------------------------------------------------------------

  ! write POSCAR
  infile = TRIM(infile)//'.out'
  CALL vasp_write( infile , geom , stat )
  CLOSE(42)

  ! deallocate stuff
  DEALLOCATE( geom%atm )
END PROGRAM run
