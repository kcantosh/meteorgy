MODULE accounting
! ----------------------------------------------------------------------------------------
! Sorting, reordering, comparisons
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
USE types
IMPLICIT NONE
! ------------------------------------------------------------------------------
! DEFINITIONS
! ===========
! nat   -  number of atoms
! mask  -  integer array that consists of all integers from 1 to nat, without
!          redundancies
! selec -  subset of mask
! key   -  choice of quantity for sorting (details below)
! 
! ------------------------------------------------------------------------------

 CONTAINS

  SUBROUTINE sort_atoms( atm , mode , threshold )
    ! --------------------------------------------------------------------------
    ! SORT ATOMS BY COORDINATES, ELEMENTS AND TAGS
    ! ALOGRITHM: insertion sort, O(n^2)
    !
    ! set the 'mode' to define sort behavior
    !  0: sort by coordinates (mix elements)
    !  1: sort by element, then by coordinates
    !  2: only sort elements (keep input order for elements, good for VASP)
    !  3: sort by tag
    !
    ! below a given threshold, coordinate is taken as equivalent. Kick yourself
    ! out
    ! 
    ! Performance: if ever used for large systems (>> 100 atoms), implement
    !              counting sort for elements and merge sort for everything
    !              else. Merge sort is better for partially ordered systems
    ! --------------------------------------------------------------------------

    ! dummies
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    INTEGER    , INTENT(IN)    :: mode
    REAL(dp)   , INTENT(INOUT) , OPTIONAL :: threshold(3)

    REAL(dp)   :: thresh(3)


    ! set threshold
    thresh = 0.0D0
    IF( PRESENT(threshold) ) thresh = threshold

    IF( mode == 0 .OR. mode == 1 ) THEN          ! sort by coordinates
      CALL sort_one( '3-' , atm )
    END IF

    IF( mode == 1 .OR. mode == 2 ) THEN           ! sort by element
      CALL sort_one( 'e+' , atm )
    END IF

    IF( mode == 3 ) THEN                          ! sort by tags
      CALL sort_one( 't+' , atm )
    END IF
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE sort_atoms


  SUBROUTINE sort_one( key , atm )
    ! --------------------------------------------------------------------------
    ! Sort atoms with insertion sort, O(n^2)
    !
    ! key      - two character instruction to sort, i.e. 'y+'
    ! key(1:1) - sort by, element (e), tag (t) or coordinates (x,y,z)
    ! key(2:2) - sort in ascending order (+) or descending order (-)
    ! atm: array of atoms
    ! --------------------------------------------------------------------------
    CHARACTER(LEN=2) , INTENT(IN)    :: key
    TYPE(atom)       , INTENT(INOUT) :: atm(:)

    ! temporary copy
    TYPE(atom) :: at

    ! control
    INTEGER          :: i,j
    CHARACTER(LEN=2) :: is_key ! insertion sort key

    ! check key(1)
    IF( key(1:1) /= 'e' .AND. key(1:1) /= 't' .AND.   &
        key(1:1) /= '1' .AND. key(1:1) /= '2' .AND. key(1:1) /= '3' ) THEN
      WRITE(stderr,'(A)') '[sort one] ERROR: unknown sort key ('//key//')'
      RETURN
    END IF

    ! check key(2), set up key(s)
    IF( key(2:2) == '+' ) THEN
      is_key = key(1:1)//'>'
    ELSE IF( key(2:2) == '-' ) THEN
      is_key = key(1:1)//'<'
    ELSE
      WRITE(stderr,'(A)') '[sort one] ERROR: unknown sort key ('//key//')'
      RETURN
    END IF


    ! Sort atoms with insertion sort, O(n^2)
    DO i = 2,SIZE(atm)
      at = atm(i)
      j = i-1
      DO WHILE( compare( is_key , atm(j) , at ) .AND. j > 0 ) ! move
        atm(j+1) = atm(j)
        j = j-1
      ENDDO
      atm(j+1) = at       ! return
    ENDDO

  CONTAINS
    FUNCTION compare( key , at1 , at2 )
      CHARACTER(LEN=2) , INTENT(in) :: key
      TYPE(atom)       , INTENT(in) :: at1 , at2
      LOGICAL :: compare
  
      SELECT CASE(key)
        CASE('e>') ; compare = at1%e     > at2%e 
        CASE('e<') ; compare = at1%e     < at2%e
        CASE('e=') ; compare = at1%e    == at2%e
        CASE('t>') ; compare = at1%t     > at2%t
        CASE('t<') ; compare = at1%t     < at2%t
        CASE('t=') ; compare = at1%t    == at2%t
        CASE('1>') ; compare = at1%r(1)  > at2%r(1)
        CASE('1<') ; compare = at1%r(1)  < at2%r(1)
        CASE('1=') ; compare = at1%r(1) == at2%r(1)
        CASE('2>') ; compare = at1%r(2)  > at2%r(2)
        CASE('2<') ; compare = at1%r(2)  < at2%r(2)
        CASE('2=') ; compare = at1%r(2) == at2%r(2)
        CASE('3>') ; compare = at1%r(3)  > at2%r(3)
        CASE('3<') ; compare = at1%r(3)  < at2%r(3)
        CASE('3=') ; compare = at1%r(3) == at2%r(3)
      END SELECT
      ! ------------------------------------------------------------------ END
    END FUNCTION compare
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE sort_one
  
  SUBROUTINE invert_order( atm )
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    TYPE(atom) :: at
    INTEGER    :: i,j,nat
    nat = SIZE(atm)
    DO i = 1,nat/2
      j = nat+1-i
      at = atm(j)
      atm(j) = atm(i)
      atm(i) = at
    ENDDO
  END SUBROUTINE invert_order


  SUBROUTINE compare_atoms( atm , refatm , mask , orphan , parent )
    ! --------------------------------------------------------------------------
    ! Compare two similar systems. Identify new, matching and missing atoms
    !
    ! mask:  matching atoms - useful to reorder atoms in atm consistent with
    !        refatm
    ! orphan:  new atoms in atm  /  missing atoms in refatm
    ! parent:  missing atoms in atm  /  new atoms in refatm
    !
    ! --------------------------------------------------------------------------
    TYPE(atom) , INTENT(IN)                :: atm(:) , refatm(:)
    INTEGER    , INTENT(OUT) , ALLOCATABLE :: mask(:)
    ! list of remainder atoms (no close counterpart)
    INTEGER    , INTENT(OUT) , ALLOCATABLE :: orphan(:) , parent(:)

    REAL(dp) :: dist2  ( SIZE(atm)    )
    REAL(dp) :: bestval( SIZE(refatm) )
    INTEGER  :: bestloc( SIZE(refatm) )
    INTEGER  :: iorphan( SIZE(atm)    )
    INTEGER  :: iparent( SIZE(refatm) )

    REAL(dp) :: tmpr(3)

    LOGICAL  :: test( SIZE(atm) )
    INTEGER  :: b,i,j,k,o,p
    INTEGER  :: nat , refnat

    nat    = SIZE(atm)
    refnat = SIZE(refatm)

    ! deallocation
    IF( ALLOCATED(mask) ) DEALLOCATE(mask)
    ALLOCATE(mask( SIZE(atm) ))

    ! get location of shortest distance atoms
    DO i=1,refnat
      DO j=1,nat
        dist2(j) =  ( atm(j)%r(1) - refatm(i)%r(1) )**2 + &
                    ( atm(j)%r(2) - refatm(i)%r(2) )**2 + &
                    ( atm(j)%r(3) - refatm(i)%r(3) )**2
      ENDDO
      bestloc(i) = MINLOC( dist2(:) , 1 )
      bestval(i) = SQRT( dist2(bestloc(i)) )
    ENDDO

    ! clear redundant values
    ! - keep closest value, reject other one (set to 0)
    ! - note other as parent
    ! - if a redundant value occurs, then one value between 1-nat in bestloc(:) is missing
    ! - values in test array remain true if orphans
    test(:) = .TRUE.
    iorphan(:) = 0
    iparent(:) = 0
    p = 0
    DO i=1,refnat
      b = bestloc(i)
      IF( test(b) ) THEN
        test(b) = .FALSE.
      ELSE
        DO j = 1 , i-1
          IF( b == bestloc(j) ) THEN
            p = p + 1
            IF( bestval(i) < bestval(j) ) THEN
              bestloc(j) = 0
              iparent(p) = j
            ELSE
              bestloc(i) = 0
              iparent(p) = i
            END IF
          END IF
        ENDDO
      END IF
    ENDDO
    ! set up orphans
    o = 0
    DO i = 1 , nat
      IF( test(i) ) THEN
        o = o + 1
        iorphan(o) = i
      END IF
    ENDDO

    ! SET UP MASK
    ! shift gaps by parents
    j = 1
    DO i = 1,refnat
      mask(j) = bestloc(i)
      IF( bestloc(i) /= 0 ) j = j + 1
    ENDDO

    ! append orphans
    mask(j:nat) = iorphan(1:o)

    ! set parents for output
    IF( ALLOCATED(parent) ) DEALLOCATE(parent)
    IF( p > 0 ) THEN
      ALLOCATE( parent(p) )
      parent(:) = iparent(1:p)
    END IF

    ! set orphans for output
    IF( ALLOCATED(orphan) ) DEALLOCATE(orphan)
    IF( o > 0 ) THEN
      ALLOCATE( orphan(o) )
      orphan(:) = iorphan(1:o)
    END IF
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE compare_atoms


  SUBROUTINE shuffle_list( mask )
    ! --------------------------------------------------------------------------
    ! RETURN A MASK IN RANDOM ORDER (modern Fischer-Yates)
    ! --------------------------------------------------------------------------
    INTEGER , INTENT(INOUT) :: mask(:)
    INTEGER :: i , rn , tmp
    REAL(dp) :: rnum( SIZE(mask) )
    CALL rnginit()
    CALL RANDOM_NUMBER( rnum )

    DO i = SIZE(mask), 2 , -1
      ! pick random from remainder
      rn = CEILING(rnum(i)*i)

      ! swap
      tmp = mask(i)
      mask(i) = mask(rn)
      mask(rn) = tmp
    ENDDO
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE shuffle_list


  SUBROUTINE mask2tag( mask , atm , stat )
    ! --------------------------------------------------------------------------
    ! write content of mask into the tag field of atoms
    ! --------------------------------------------------------------------------
    INTEGER    , INTENT(IN)    :: mask(:)
    TYPE(atom) , INTENT(INOUT) :: atm(:)
    INTEGER    , INTENT(OUT)   :: stat
    INTEGER    :: i

    IF( SIZE(mask) /= SIZE(atm) ) THEN
      WRITE(stderr,'(A)') '[mask2tag] Cannot apply mask - size not equal!'
      RETURN
    END IF

    DO i=1,SIZE(atm)
      atm(i)%t = mask(i)
    ENDDO
    ! ---------------------------------------------------------------------- END
  END SUBROUTINE mask2tag

  
  FUNCTION extract_atoms( selec , set ) RESULT( subset )
    ! --------------------------------------------------------------------------
    ! Return atoms given by selection
    ! --------------------------------------------------------------------------
    INTEGER    , INTENT(IN) :: selec(:)
    TYPE(atom) , INTENT(IN) :: set(:)
    TYPE(atom) :: subset( SIZE(selec) )
    INTEGER    :: i

    ! extract elements and positions
    DO i = 1,SIZE(selec)
      subset(i) = set(selec(i))
    ENDDO
  END FUNCTION extract_atoms


  SUBROUTINE check_mask( mask )
    INTEGER , INTENT(in) :: mask(:)
    INTEGER :: i,j,n

    n = SIZE(mask)

    DO i = 1,n
      IF( mask(i) > n .OR. mask(i) < 1 ) WRITE(stderr,'(A,I4,A,I4,A)') '[check mask] ERROR: Entry',i,' out of range,',mask(i),'.'
      DO j = 1,i-1
        IF( mask(i) == mask(j) ) WRITE(stderr,'(A,3I4)') '[check mask] ERROR: Redundancy,',mask(i),i,j
      ENDDO
    ENDDO

  END SUBROUTINE check_mask


  SUBROUTINE make_mask( nat , mask , selec )
    ! --------------------------------------------------------------------------
    ! bring all atoms in selec to the top of mask
    ! WARNING: assumes correct mask
    ! --------------------------------------------------------------------------
    INTEGER , INTENT(IN)  :: nat
    INTEGER , INTENT(OUT) , ALLOCATABLE ::  mask( : )
    INTEGER , INTENT(IN)  , ALLOCATABLE :: selec( : )
    INTEGER :: snat , i , j

    IF( ALLOCATED(mask) ) DEALLOCATE( mask )
    snat = SIZE(selec)
    
    ! initialize mask
    ALLOCATE( mask(nat) )
    mask(:) = 0

    ! gaps in old mask
    DO i = 1,snat
      mask(selec(i)) = -1
    ENDDO

    ! set up remainder
    j=nat
    DO i = nat,1,-1
      mask(j) = i
      IF( mask(i) == 0 ) j=j-1
    ENDDO

    ! selec on top
    DO i = 1,snat
      mask(i) = selec(i)
    ENDDO
  END SUBROUTINE make_mask


  FUNCTION print_integer( array , separator ) RESULT( list )
    ! --------------------------------------------------------------------------
    ! return a printable list if integer array
    ! --------------------------------------------------------------------------
    INTEGER , INTENT(IN) , ALLOCATABLE          :: array(:)
    CHARACTER(LEN=1) , INTENT(in) , OPTIONAL    :: separator
    CHARACTER(LEN=1)                            :: sep
    CHARACTER(LEN=:) , ALLOCATABLE              :: list , ilist
    CHARACTER(LEN=str_)                         :: tmp

    INTEGER :: nat , i

    ! set separator
    sep = ' ' 
    IF( PRESENT ( separator ) ) sep = separator

    nat = MERGE( SIZE(array) , 0 , ALLOCATED(array) )

    ALLOCATE( CHARACTER(LEN=str_*nat) :: ilist )
    ilist = ''

    ! fill list
    DO i = 1,nat
      WRITE(tmp,*) array(i)
      ilist = TRIM(ilist)//sep//ADJUSTL(tmp)
    ENDDO

    ! make output
    list = TRIM(ilist(2:))

  END FUNCTION print_integer


END MODULE accounting
