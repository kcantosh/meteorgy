MODULE types
! ----------------------------------------------------------------------------------------
! I/O routines to access/write vasp geometries and xyz files
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------

  USE params
  USE algebra3d , ONLY: cross3d , matinv3d
  IMPLICIT NONE

  INTEGER , PARAMETER  ::  maxsorts_= 100       ! maximum amount of atom sorts

  INTEGER                   , PARAMETER :: list_length=111 , sym_length=2
  CHARACTER(LEN=sym_length) , PARAMETER :: anon_element='Xx'
  REAL(dp)                  , PARAMETER :: anon_weight =0.0D0
  CHARACTER(LEN=sym_length) , PRIVATE   :: list_symbols(list_length)
  REAL(dp)                  , PRIVATE   :: list_weights(list_length)

  DATA list_symbols / &
    'H ', 'He', 'Li', 'Be', 'B ', 'C ', 'N ', 'O ', 'F ', 'Ne', &
    'Na', 'Mg', 'Al', 'Si', 'P ', 'S ', 'Cl', 'Ar', 'K ', 'Ca', &
    'Sc', 'Ti', 'V ', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', &
    'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y ', 'Zr', &
    'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', &
    'Sb', 'Te', 'I ', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', &
    'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', &
    'Lu', 'Hf', 'Ta', 'W ', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', &
    'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', &
    'Pa', 'U ', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', &
    'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', &
    'Rg'/

  DATA list_weights / &
      1.0079D0,   4.0026D0,   6.9410D0,   9.0122D0,  10.8110D0,  12.0107D0, &
     14.0067D0,  15.9994D0,  18.9984D0,  20.1797D0,  22.9897D0,  24.3050D0, &
     26.9815D0,  28.0855D0,  30.9738D0,  32.0650D0,  35.4530D0,  39.9480D0, &
     39.0983D0,  40.0780D0,  44.9559D0,  47.8670D0,  50.9415D0,  51.9961D0, &
     54.9380D0,  55.8450D0,  58.9332D0,  58.6934D0,  63.5460D0,  65.3900D0, &
     69.7230D0,  72.6400D0,  74.9216D0,  78.9600D0,  79.9040D0,  83.8000D0, &
     85.4678D0,  87.6200D0,  88.9059D0,  91.2240D0,  92.9064D0,  95.9400D0, &
     98.0000D0, 101.0700D0, 102.9055D0, 106.4200D0, 107.8682D0, 112.4110D0, &
    114.8180D0, 118.7100D0, 121.7600D0, 127.6000D0, 126.9045D0, 131.2930D0, &
    132.9055D0, 137.3270D0, 138.9055D0, 140.1160D0, 140.9077D0, 144.2400D0, &
    145.0000D0, 150.3600D0, 151.9640D0, 157.2500D0, 158.9253D0, 162.5000D0, &
    164.9303D0, 167.2590D0, 168.9342D0, 173.0400D0, 174.9670D0, 178.4900D0, &
    180.9479D0, 183.8400D0, 186.2070D0, 190.2300D0, 192.2170D0, 195.0780D0, &
    196.9665D0, 200.5900D0, 204.3833D0, 207.2000D0, 208.9804D0, 209.0000D0, &
    210.0000D0, 222.0000D0, 223.0000D0, 226.0000D0, 227.0000D0, 232.0381D0, &
    231.0359D0, 238.0289D0, 237.0000D0, 244.0000D0, 243.0000D0, 247.0000D0, &
    247.0000D0, 251.0000D0, 252.0000D0, 257.0000D0, 258.0000D0, 259.0000D0, &
    262.0000D0, 261.0000D0, 262.0000D0, 266.0000D0, 264.0000D0, 277.0000D0, &
    268.0000D0, 270.0000D0, 272.0000D0 /

! ----------------------------------------------------------------------------------------
! COMMON DATA STRUCTURES
! ... to decrease argument lists but kept as simple as possible
! ----------------------------------------------------------------------------------------

  TYPE atom
    INTEGER  :: e        ! element
    INTEGER  :: t        ! tag (anything - enumerator for partitioning, reordering, ...)
    REAL(dp) :: r(3)     ! coordinate, can be anything (fractional, cartesian, ...)
    REAL(dp) :: v(3)     ! velocities (hopefully given in cartesian coordinates)
    LOGICAL  :: c        ! constraint (false = fixed, true = free)
  END TYPE atom

  TYPE pbc
    INTEGER  :: nat      ! number of atoms
    CHARACTER(LEN=:) , ALLOCATABLE   &
             :: comment  ! comment

    INTEGER  :: sys      ! coordinate system used for atoms
                         ! CAREFUL: unit cell data remains untouched, the
                         ! lower dimensionality unit cell can be calculated
                         ! on-the-fly from the unit cell with mix_box
                         !
                         ! [0] any       (cartesian)
                         ! [1] wire/tube (mixed, b=y and c=z)
                         !     [4]              (b=x and c=z)
                         !     [7]              (b=x and c=y)
                         ! [2] slab      (mixed, c=z)
                         !     [5]              (b=x)
                         !     [8]              (b=y)
                         ! [3] bulk      (direct/fractional)
                         ! [4] wire/tube (mixed, b=x and c=z)
                         ! [6] undefined

    LOGICAL  :: lc &     ! use constraints
             ,  lv       ! use velocities
    REAL(dp) :: box(3,3) ! unit cell
    TYPE(atom) , ALLOCATABLE   &
             :: atm(:)   ! atoms
  END TYPE pbc


 CONTAINS


! ----------------------------------------------------------------------------------------
! CONVERT BETWEEN ELEMENT SYMBOLS AND ATOMIC NUMBER
! ----------------------------------------------------------------------------------------

  ELEMENTAL PURE FUNCTION atomic_number( ielement_symbol )
  ! Returns atomic number of element given by symbol
    INTEGER                                 :: atomic_number
    CHARACTER(LEN=sym_length) , INTENT(IN)  :: ielement_symbol
    CHARACTER(LEN=sym_length)               :: int_element_symbol   ! internal modifiable symbol
    INTEGER                                 :: counter , tmp

    int_element_symbol = ielement_symbol
    CALL clean_symbol( int_element_symbol )

    atomic_number = 0
    DO counter=1,list_length
     IF( int_element_symbol == list_symbols(counter) ) THEN
       atomic_number = counter
       RETURN
     END IF
    END DO
  END FUNCTION atomic_number


  ELEMENTAL PURE FUNCTION element_symbol( iatomic_number )
    ! Returns element symbol, of element with input atomic number
    CHARACTER(LEN=sym_length)       :: element_symbol
    INTEGER, INTENT(IN)             :: iatomic_number

    IF( iatomic_number <= 0 .OR. iatomic_number > list_length ) THEN
      element_symbol = anon_element
    ELSE
      element_symbol = list_symbols( iatomic_number )
    END IF
  END FUNCTION element_symbol


  ELEMENTAL PURE FUNCTION element_weight( iatomic_number )
    ! Returns element symbol, of element with input atomic number
    REAL(dp)                        :: element_weight
    INTEGER, INTENT(IN)             :: iatomic_number

    IF( iatomic_number <= 0 .OR. iatomic_number > list_length ) THEN
      element_weight = anon_weight
    ELSE
      element_weight = list_weights( iatomic_number )
    END IF
  END FUNCTION element_weight


  ELEMENTAL SUBROUTINE clean_symbol( ioelement_symbol )
    ! First letter upper case, second letter lower case
    CHARACTER(LEN=sym_length), INTENT(INOUT) :: ioelement_symbol
    INTEGER                         :: counter , tmp
    LOGICAL                         :: integrity

    ! worst case
    integrity = .FALSE.

    ! convert first letter to upper case
    tmp = IACHAR(ioelement_symbol(1:1))
    IF( tmp > 96 .AND. tmp <= 122 ) THEN
      ioelement_symbol(1:1) = ACHAR(tmp-32)
    ELSE IF( tmp < 64 .AND. tmp >= 90 ) THEN
      ioelement_symbol(1:1) = anon_element(1:1)
    END IF
    ! convert second letter to lower case
    tmp = IACHAR(ioelement_symbol(2:2))
    IF( tmp > 64 .AND. tmp <= 90 ) THEN
      ioelement_symbol(1:1) = ACHAR(tmp+32)
    ELSE IF( tmp < 96 .AND. tmp >= 122 ) THEN
      ioelement_symbol(2:2) = anon_element(2:2)
    END IF
    ! check if the element exists
    DO counter=1,list_length
      IF( ioelement_symbol == list_symbols(counter) ) THEN
        integrity = .TRUE.
        EXIT
      END IF
    END DO
    IF( .NOT. integrity ) THEN
      ioelement_symbol = anon_element
    END IF
  END SUBROUTINE clean_symbol


! -----------------------------------------------------------------------------
! CONVERT BETWEEN pbc FORMATS
! -----------------------------------------------------------------------------

  SUBROUTINE pbc_convert( geom , sys , stat )
    ! -------------------------------------------------------------------------
    ! convert between available coordinate systems
    ! - see pbc type declaration for details
    ! -------------------------------------------------------------------------
    TYPE(pbc) , INTENT(INOUT) :: geom
    INTEGER   , INTENT(IN)    :: sys
    INTEGER   , INTENT(OUT)   :: stat
    INTEGER  :: i
    REAL(dp) :: trans(3,3)

    stat = 0

    ! sanity check
    IF( geom%sys < 0 .OR. geom%sys == 6 .OR. geom%sys > 8 .OR.  &
        sys < 0 .OR. sys == 6 .OR. sys > 8 ) THEN
      WRITE(stderr,'(A)') "[pbc convert] ERROR: system type unknown!"
      stat = 1
      RETURN 
    ENDIF

    ! nothing to convert
    IF( geom%sys == sys ) RETURN 

    ! multiply the transformation matrix to new sys ... 
    trans =  matinv3d( mix_box( geom%box , sys ) )

    ! ... with inverse of transformation matrix to old system!
    trans =  MATMUL(  trans  ,  mix_box( geom%box , geom%sys ) )

    ! transform coordinates directly.
    DO i=1,geom%nat
      geom%atm(i)%r = MATMUL( trans , geom%atm(i)%r )
    ENDDO

    geom%sys = sys

  END SUBROUTINE pbc_convert


  PURE FUNCTION mix_box( box , sys ) RESULT( mix )
    ! --------------------------------------------------------------------------
    ! lower dimensionality unit cell. removed cell vectors are replaced by
    ! ortho-normalized vectors.
    ! --------------------------------------------------------------------------

    REAL(dp) , INTENT(IN)  :: box(3,3)
    INTEGER  , INTENT(IN)  :: sys
    REAL(dp)               :: mix(3,3) , vec(3)
    SELECT CASE( sys )

      CASE( 0 )
        mix = RESHAPE( (/1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0/), (/3, 3/) )

      CASE( 1 )
        mix(:,1) = box(:,1)
        vec      = cross3d( box(:,1) , box(:,2) )
        mix(:,3) = vec / SQRT( SUM( vec**2 ) )
        vec      = cross3d( mix(:,3) , mix(:,1) )
        mix(:,2) = vec / SQRT( SUM( vec**2 ) )

      CASE( 4 )
        mix(:,2) = box(:,2)
        vec      = cross3d( box(:,2) , box(:,3) )
        mix(:,1) = vec / SQRT( SUM( vec**2 ) )
        vec      = cross3d( mix(:,1) , mix(:,2) )
        mix(:,3) = vec / SQRT( SUM( vec**2 ) )

      CASE( 7 )
        mix(:,3) = box(:,3)
        vec      = cross3d( box(:,3) , box(:,1) )
        mix(:,2) = vec / SQRT( SUM( vec**2 ) )
        vec      = cross3d( mix(:,2) , mix(:,3) )
        mix(:,1) = vec / SQRT( SUM( vec**2 ) )

      CASE( 2 )
        mix(:,1) = box(:,1)
        mix(:,2) = box(:,2)
        vec      = cross3d( box(:,1) , box(:,2) )
        mix(:,3) = vec / SQRT( SUM( vec**2 ) )

      CASE( 5 )
        mix(:,2) = box(:,2)
        mix(:,3) = box(:,3)
        vec      = cross3d( box(:,2) , box(:,3) )
        mix(:,1) = vec / SQRT( SUM( vec**2 ) )

      CASE( 8 )
        mix(:,3) = box(:,3)
        mix(:,1) = box(:,1)
        vec      = cross3d( box(:,3) , box(:,1) )
        mix(:,2) = vec / SQRT( SUM( vec**2 ) )

      CASE( 3 )
        mix = box

    END SELECT

  END FUNCTION mix_box
  
  
  FUNCTION get_sys( plane , wire , crystal ) RESULT( sys )
    ! --------------------------------------------------------------------------
    ! return number of system, used for external procedures that do not want to
    ! bother with changing defaults. plane or wire according to description
    ! above
    ! > intended to give only one or no input
    ! --------------------------------------------------------------------------
    INTEGER , OPTIONAL , INTENT(IN) :: plane , wire , crystal
    INTEGER :: sys , tmp

    IF( PRESENT(plane) ) THEN
      sys = (MODULO(plane,3)+1)*3-1
    ELSE IF( PRESENT(wire) ) THEN
      sys = (MODULO(wire-1,3)+1)*3-2
    ELSE IF( PRESENT(crystal) ) THEN
      sys = 3
    ELSE
      sys = 0
    ENDIF
  END FUNCTION get_sys


  SUBROUTINE makebox( box , lrhom , separation )
    ! --------------------------------------------------------------------------
    ! create unsymmetric boxes with near cubic or rhombohedral shape and
    ! given separation
    !  - separation in angstrom is the nearest distance between two images
    !  - rhombohedron is equivalent fcc closest sphere packing
    !    (best volume/distance ratio)
    !  - symmetry broken by diff-parameter. Workaround for known bug in VASP
    !    when using cubic sells for molecule calculations
    ! --------------------------------------------------------------------------
    REAL(dp) , PARAMETER :: sqrt2 = 1.4142135623730950488D0 , diff=0.05

    ! modes - 0: custom cell (already given), 1: appr. cubic cell, 2: appr. orthorhombic cell
    LOGICAL  , INTENT(IN)  :: lrhom
    REAL(dp) , INTENT(IN)  :: separation

    REAL(dp) , INTENT(OUT) :: box(3,3)

    ! set cell
    IF( lrhom ) THEN
      ! ~ rhombohedron
      box(:,:) = 0.0D0
      box(2,1) = separation/sqrt2*(1.0D0-diff)
      box(3,1) = separation/sqrt2*(1.0D0-diff)
      box(1,2) = separation/sqrt2*(1.0D0)
      box(3,2) = separation/sqrt2*(1.0D0)
      box(1,3) = separation/sqrt2*(1.0D0+diff)
      box(2,3) = separation/sqrt2*(1.0D0+diff)
    ELSE
      ! ~ cube
      box(:,:) = 0.0D0
      box(1,1) = separation*(1.0D0-diff)
      box(2,2) = separation*(1.0D0)
      box(3,3) = separation*(1.0D0+diff)
    ENDIF

  END SUBROUTINE makebox


! ----------------------------------------------------------------------------------------
! NICE PRINT OUT
! ----------------------------------------------------------------------------------------

  SUBROUTINE box_print( box )
    ! unit cell geometry data
    REAL(dp) , INTENT(IN) :: box(3,3)
    REAL(dp) :: value(3) , angle(3)
    value(1) = SQRT(SUM( (box(:,1)**2) ))
    value(2) = SQRT(SUM( (box(:,2)**2) ))
    value(3) = SQRT(SUM( (box(:,3)**2) ))

    angle(1) = ACOS( SUM(box(:,2)*box(:,3))/(value(2)*value(3)) )
    angle(2) = ACOS( SUM(box(:,1)*box(:,3))/(value(1)*value(3)) )
    angle(3) = ACOS( SUM(box(:,1)*box(:,2))/(value(1)*value(2)) )
    angle    = 180.0D0/PI_ * angle

    WRITE(*,'(A12,3A13,A11)') 'x' , 'y' , 'z' , 'r' , 'ang'
    WRITE(*,'(5X,58("-"))')
    WRITE(*,'(2X,"a",3F13.8,F11.4,F10.2)') box(:,1) , value(1) , angle(1)
    WRITE(*,'(2X,"b",3F13.8,F11.4,F10.2)') box(:,2) , value(2) , angle(2)
    WRITE(*,'(2X,"c",3F13.8,F11.4,F10.2)') box(:,3) , value(3) , angle(3)
  END SUBROUTINE box_print


  SUBROUTINE atom_print( atm , lconstr , lvel )
    ! unit cell geometry data
    TYPE(atom) , INTENT(IN) :: atm(:)
    LOGICAL    , INTENT(IN) :: lconstr , lvel
    CHARACTER(LEN=3) :: coord(3)
    INTEGER :: nat , i
    nat = SIZE(atm)
    coord = (/ 'a/x' , 'b/y' , 'c/z' /)

    IF( lvel .AND. lconstr ) THEN
      WRITE(*,'(2X,A3,2X,A4,2X,A4,A7,2A13,2X,3A13,A12)') '#','tag','sym.',coord,'v_x','v_y','v_z','constr.'
      WRITE(*,'(3X,99("-"))')
      DO i = 1,nat
        WRITE(*,'(I5,2X,I4,2X,A2,3F13.8,2X,3F13.8,2X,L2)') &
          i , atm(i)%t , element_symbol( atm(i)%e ) , atm(i)%r , atm(i)%v , atm(i)%c
      END DO
    ELSEIF( lvel .AND. .NOT. lconstr ) THEN
      WRITE(*,'(2X,A3,2X,A4,2X,A4,A7,2A13,2X,3A13)') '#','tag','sym.',coord,'v_x','v_y','v_z'
      WRITE(*,'(3x,91("-"))')
      DO i = 1,nat
        WRITE(*,'(I5,2X,I4,2X,A2,3F13.8,2X,3F13.8)') &
          i , atm(i)%t , element_symbol( atm(i)%e ) , atm(i)%r , atm(i)%v
      END DO
    ELSEIF( .NOT. lvel .AND. lconstr ) THEN
      WRITE(*,'(2X,A3,2X,A4,2X,A4,A7,3A13)') '#','tag','sym.',coord,'constr.'
      WRITE(*,'(3X,59("-"))')
      DO i = 1,nat
        WRITE(*,'(I5,2X,I4,2X,A2,3F13.8,2X,L2)') &
          i , atm(i)%t , element_symbol( atm(i)%e ) , atm(i)%r , atm(i)%c
      END DO
    ELSE
      WRITE(*,'(2X,A3,2X,A4,2X,A4,A7,2A13)') '#','tag','sym.',coord
      WRITE(*,'(3X,51("-"))')
      DO i = 1,nat
        WRITE(*,'(I5,2X,I4,2X,A2,3F13.8)') &
          i , atm(i)%t , element_symbol( atm(i)%e ) , atm(i)%r
      END DO
    END IF
  END SUBROUTINE atom_print

  SUBROUTINE pbc_print( geom )
    TYPE(pbc) , INTENT(IN) :: geom
    CALL box_print( geom%box )
    CALL atom_print( geom%atm , geom%lc , geom%lv )
  END SUBROUTINE pbc_print

END MODULE types
