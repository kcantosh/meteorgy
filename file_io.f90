MODULE file_io
USE params
USE types  , ONLY: pbc , atom
! ----------------------------------------------------------------------------------------
! I/O routines to access/write vasp geometries and xyz files
! Copyright (C) 2018 Adrian Hühn
! 
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT ANY
! WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
! PARTICULAR PURPOSE.  See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with this
! program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
! Fifth Floor, Boston, MA  02110-1301, USA.
! ----------------------------------------------------------------------------------------
 IMPLICIT NONE
 CONTAINS

  SUBROUTINE open_infile( filename , funit , stat )
    ! ------------------------------------------------------------------------------------
    ! OPEN INPUT FILE OR READ FROM STDIN
    ! ------------------------------------------------------------------------------------

    CHARACTER(LEN=*)  , INTENT(IN)    :: filename
    INTEGER           , INTENT(OUT)   :: funit
    INTEGER           , INTENT(OUT)   :: stat

    CHARACTER(LEN=:)  , ALLOCATABLE   :: fname
    LOGICAL                           :: exists ! file

    fname = TRIM(ADJUSTL(filename))

    IF( fname == '-' ) THEN

      ! read from stdin
      funit = stdin_
      WRITE(stdout,'(A)') "Read from standard input ..."

    ELSE
      funit = inunit
      INQUIRE(file=fname,exist=exists)
      IF(.NOT.exists) THEN
        WRITE(stderr,'(A)') "[open] ERROR: File '"//fname//"' does not exist."
        stat = 1
        RETURN
      ENDIF

      ! open file
      OPEN(unit=funit,file=fname,status="OLD",iostat=stat)
      IF(stat/=0) THEN
        WRITE(stderr,'(A)') "[open] ERROR: Cannot open "//fname//" (input file)."
        CLOSE(funit)
        RETURN
      ENDIF

    ENDIF

    stat = 0

  END SUBROUTINE open_infile


  SUBROUTINE open_outfile( filename , funit , stat )
    ! ------------------------------------------------------------------------------------
    ! OPEN OUTPUT FILE OR WRITE TO STDOUT
    ! ------------------------------------------------------------------------------------

    CHARACTER(LEN=*)  , INTENT(IN)    :: filename
    INTEGER           , INTENT(OUT)   :: funit
    INTEGER           , INTENT(OUT)   :: stat

    CHARACTER(LEN=:)  , ALLOCATABLE   :: fname

    fname = TRIM(ADJUSTL(filename))
!WRITE(*,*) 'DEBUG0'

    IF( fname == '-' ) THEN
    ! write to stdout
      funit = stdout_
    ELSE
!WRITE(*,*) 'DEBUG1'
    ! open file
      funit = outunit
      OPEN(unit=funit,file=fname,status="REPLACE",iostat=stat)
      IF(stat/=0) THEN
        WRITE(stderr,'(A)') "[open] ERROR: Cannot replace '"//fname//"'."
        RETURN
      ENDIF
!WRITE(*,*) 'DEBUG4'

    ENDIF
!WRITE(*,*) 'DEBUG5'

  END SUBROUTINE open_outfile


  SUBROUTINE vasp_read( filename , geom , stat )
    USE algebra3d,  ONLY: det3d
    USE types, ONLY: maxsorts_ , atomic_number , element_symbol
    ! file unit number
    CHARACTER(LEN=*) , INTENT(IN)  :: filename
    ! unit cell geometry data
    TYPE(pbc)        , INTENT(OUT) :: geom
    ! error status
    INTEGER          , INTENT(OUT) :: stat

    ! internally corrected filename
    CHARACTER(LEN=:) , ALLOCATABLE :: fname
    ! temporary line
    CHARACTER(LEN=lstr_)      :: tmpline
    ! scaling factor
    REAL(dp)                  :: scaling
    ! number of atoms
    INTEGER                   :: nat
    ! number of atom sorts
    INTEGER                   :: atm_sorts
    ! contained element list, symbols
    CHARACTER(LEN=2)          :: elem_sym(MAXSORTS_)
    ! contained element list, atomic number
    INTEGER                   :: elem_num(MAXSORTS_)
    ! number of atoms per element
    INTEGER                   :: nat_elem(MAXSORTS_)

    ! counters
    INTEGER                   :: i , j , k
    ! unit cell volume
    REAL(dp)                  :: volume
    ! temporary coordinates and constraints
    REAL(dp)                  :: vasp_r(3)
    LOGICAL                   :: vasp_c(3)
    ! exceptions
    CHARACTER(LEN=str_)       :: efmt

    ! file unit
    INTEGER                   :: funit

    ! ------------------------------------------------------------------------------------
    ! OPEN INPUT FILE
    ! ------------------------------------------------------------------------------------
    CALL open_infile( filename , funit , stat )
    IF(stat/= 0) GOTO 73

    ! ------------------------------------------------------------------------------------
    ! READING FIRST FIVE LINES (comment, scaling, unit cell)
    ! ------------------------------------------------------------------------------------

    efmt = '(I5,1X,A)'
    tmpline = ''
    scaling = 0.0D0
    READ(funit,'(A)',iostat=stat) tmpline       ! line 1
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading comment!"; GOTO 73; ENDIF
    geom%comment = TRIM(tmpline)
    READ(funit,*,iostat=stat) scaling           ! line 2
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading scaling parameter!"; GOTO 73; ENDIF
    IF(scaling==0.0D0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR scaling is zero!"; GOTO 73; ENDIF
    READ(funit,*,iostat=stat) geom%box(:,1)     ! line 3
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading cell vector a!"; GOTO 73; ENDIF
    READ(funit,*,iostat=stat) geom%box(:,2)     ! line 4
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading cell vector b!"; GOTO 73; ENDIF
    READ(funit,*,iostat=stat) geom%box(:,3)     ! line 5
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading cell vector c!"; GOTO 73; ENDIF

    ! test right hand coordinate system
    volume = det3d(geom%box)
    IF(volume<=0.0D0) THEN; WRITE(stderr,efmt) stat, &
      "[vasp read] ERROR: Linear dependencies or left-handed cell vectors"; GOTO 73; ENDIF

    ! ------------------------------------------------------------------------------------
    ! READING ELEMENT DATA
    ! ------------------------------------------------------------------------------------
    elem_sym    = element_symbol(0)
    elem_num    = 0
    nat_elem    = 0

    ! read line of contained elements and parse it from isolated array
    READ(funit,'(A)',iostat=stat) tmpline       ! line 6
    IF(stat/= 0) THEN; WRITE(stderr,efmt) stat, &
      "[vasp read] ERROR reading elements/atoms per element!"; GOTO 73; ENDIF

    READ(tmpline,*,iostat=stat) elem_sym(:)  ! read element symbols

    IF( atomic_number(elem_sym(1)) /= 0  ) THEN
    ! test whether first item is a listed element, ...

      ! get next line for number of atoms per element
      READ(funit,'(A)',iostat=stat) tmpline   ! read next line
    ELSE
      ! ... if not then ELEMENTS ARE MISSING!!!
      ! (make up some elements now)
      elem_sym(1:7) = (/ 'Ne', 'Ar', 'Kr', 'Xe', 'Rn', 'He', 'Fr' /) 
      WRITE(stderr,efmt) stat , "[vasp read] ERROR reading elements!"
      elem_sym(:) = element_symbol(0)
    END IF
    READ(tmpline,*,iostat=stat) nat_elem(:)   ! read number of atoms per element
    IF(stat >0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading atoms per element!"; GOTO 73; ENDIF

    ! ------------------------------------------------------------------------------------
    ! READ SELECTION SWITCH (constraints) AND COORDINATE SYSTEM (direct/fractional or cartesian)
    ! ------------------------------------------------------------------------------------
    geom%lc  = .FALSE.
    READ(funit,*,iostat=stat) tmpline
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading 'selective'/'basis' switch!"; GOTO 73; ENDIF
    ! check if selective (read constraints) was added
    IF( tmpline(1:1) =='s' .OR. tmpline(1:1) =='S' ) THEN
      geom%lc = .TRUE.
      READ(funit,*,iostat=stat) tmpline
      IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading 'basis' switch!"; GOTO 73; ENDIF
    END IF
    geom%sys = 3
    IF( tmpline(1:1)=='c' .OR.  &
        tmpline(1:1)=='C' .OR.  &
        tmpline(1:1)=='k' .OR.  &
        tmpline(1:1)=='K' )  geom%sys = 0
    
    ! ------------------------------------------------------------------------------------
    ! SET SCALING
    ! (negative scaling values are treated as cell volume in angstrom^3)
    ! ------------------------------------------------------------------------------------

    IF( scaling <  0.0D0 ) scaling = (ABS( scaling / volume ))**(1.0D0/3.0D0)
    geom%box = scaling * geom%box

    ! ------------------------------------------------------------------------------------
    ! GET NUMBER OF ATOMS, ALLOCATE geom, SET UP ELEMENT ARRAY
    ! ------------------------------------------------------------------------------------
    DO i=1,MAXSORTS_
      IF( elem_sym(i) == element_symbol(0) ) THEN
        atm_sorts = i-1
        EXIT
      END IF
    END DO
    ! allocate
    nat = SUM(nat_elem)
    geom % nat = nat
    ALLOCATE( geom % atm(geom%nat) )

    ! assign element list
    k=1
    ! assign elements
    DO i=1,atm_sorts
      elem_num(i) = atomic_number( elem_sym(i) )
      DO j=1,nat_elem(i) 
        geom%atm(k)%e = elem_num(i)
        k=k+1
      END DO
    END DO

    ! ------------------------------------------------------------------------------------
    ! READ ATOM LIST (with or without constraints based on geom%lc)
    ! ------------------------------------------------------------------------------------
    efmt = '(I5,1X,A,I4,A)'
    IF( geom%sys == 3 ) scaling = 1.0D0

    IF( geom%lc ) THEN
      geom % atm(:) % c = .FALSE.

      DO i=1,geom%nat
        READ(funit,*,iostat=stat) vasp_r(:) , vasp_c(:)
        IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading coordinates of atom",i,"!"; GOTO 73; ENDIF

        ! set coordinates
        geom % atm(i) % r = scaling * vasp_r

        ! set constraints
        IF( vasp_c(1) .AND. vasp_c(2) .AND. vasp_c(3) ) THEN
          geom % atm(i) % c = .TRUE.
        ELSEIF( vasp_c(1) .OR. vasp_c(2) .OR. vasp_c(3) ) THEN
          WRITE(stderr,efmt) stat,"[vasp read] WARNING: Partial constraints not supported, atom",i," assumed fixed!"
        ENDIF

      ENDDO

    ELSE
      geom % atm(:) % c = .TRUE.

      DO i=1,geom%nat
        READ(funit,*,iostat=stat) vasp_r(:)
        IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading coordinates of atom",i,"!"; GOTO 73; ENDIF

        ! set coordinates
        geom % atm(i) % r = scaling * vasp_r

      ENDDO

    ENDIF

    ! ------------------------------------------------------------------------------------
    ! READ VELOCITIES (if available, adding some cases to succeed without successful read)
    ! ------------------------------------------------------------------------------------

    DO i=1,geom%nat
      geom%atm(i)%v = 0.0D0
    ENDDO

    READ(funit,'(A)',iostat=stat) tmpline      ! read empty line
    IF(stat <0) THEN; stat=0; GOTO 23; ENDIF ! return from routine as end of file reached
    IF(stat >0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR before velocity list!"; GOTO 73; ENDIF

    ! buffer and try to read first line as floats
    READ(funit,'(A)',iostat=stat) tmpline      ! buffer first line of velocities
    IF(stat <0) THEN; stat=0; GOTO 23; ENDIF
    IF(stat >0) THEN; WRITE(stderr,efmt) stat,"[vasp read] Severe ERROR at first line of velocity list!"; GOTO 73; ENDIF
    READ(tmpline,*,iostat=stat) vasp_r(:) ! try to read first line

    ! check for NaN
    tmpline = ADJUSTL(tmpline)
    IF( tmpline(1:1) == 'N' ) THEN; stat=0; GOTO 23; ENDIF

    ! fail on first read
    IF(stat/=0) THEN; WRITE(stderr,efmt) stat,"[vasp read] ERROR reading velocity of atom!",1,"!"; GOTO 73; ENDIF

    ! go, looper
    geom%atm(1)%v = vasp_r(:)
    DO i=2,nat                              ! loop from 2 as first velocity is added manually
      READ(funit,*,iostat=stat) geom%atm(i)%v
      IF(stat>0) THEN
        WRITE(stderr,efmt) stat,"[vasp read] ERROR reading velocity of atom",i,"!"; GOTO 73
      ELSEIF(stat<0) THEN
        stat=0; GOTO 23
      ENDIF
    ENDDO

23  CLOSE(funit)
    WRITE(stdout,'(A)') "Data from '"//TRIM(filename)//"' (VASP) successfully read in."
    RETURN

    ! exception handling
73  CLOSE(funit)
    WRITE(stdout,'(A)') "No data was read from VASP input '"//TRIM(filename)//"'."
    IF( ALLOCATED( geom%atm )) DEALLOCATE( geom%atm )

  END SUBROUTINE vasp_read


  SUBROUTINE vasp_write( filename , geom , stat )
    USE types, ONLY: maxsorts_ , pbc_convert , element_symbol
    ! file unit number
    CHARACTER(LEN=*) , INTENT(IN)    :: filename
    ! unit cell geometry data
    TYPE(pbc)        , INTENT(INOUT) :: geom
    ! error status
    INTEGER          , INTENT(OUT)   :: stat
    ! ------------------------------------------------------------------------------------
    ! number of atom sorts
    INTEGER             :: atm_sorts
    ! element's atomic number
    INTEGER             :: elem_num(MAXSORTS_)
    ! element symbols
    CHARACTER(LEN=2)    :: elem_sym(MAXSORTS_)
    ! element's atomic number
    INTEGER             :: nat_elem(MAXSORTS_)

    ! ------------------------------------------------------------------------------------
    ! INTERNAL
    CHARACTER(LEN=lstr_)            :: tmpline

    ! formatting (ffrmt = float format)
    CHARACTER(LEN=str_)   :: ffrmt , frmt
    ! counters
    INTEGER             :: i , j
    ! file handling
    INTEGER             :: funit

    ! ------------------------------------------------------------------------------------
    ! ONLY PROCEED FOR geom%sys = 0 or 3
    ! ------------------------------------------------------------------------------------
    IF( geom%sys /= 3 ) THEN
      CALL pbc_convert( geom , 0 , stat )
    ENDIF

    ! ------------------------------------------------------------------------------------
    ! GET ELEMENT LIST / NUMBER OF ATOMS PER ELEMENT
    ! ------------------------------------------------------------------------------------

    ! get elements and number of atoms per element
    atm_sorts    = 0
    elem_sym(:)  = element_symbol( 0 )
    elem_num(:)  = 0
    nat_elem(:)  = 0

    j = 1
    DO i = 1 , geom%nat             ! loop through element array
      IF( elem_num(j) /= 0 .AND. elem_num(j) /= geom%atm(i)%e ) THEN
        j = j + 1
      END IF
      elem_num(j) = geom%atm(i)%e   ! assign with element number
      nat_elem(j) = nat_elem(j) + 1 ! count element
    END DO
    atm_sorts = j

    ! WARNING
    DO i = 2 , atm_sorts
      DO j = 1 , i-1
        IF( elem_num(j) == elem_num(i) ) THEN
          WRITE(stderr,'(A)') "[vasp write] WARNING: Detected multiple sets of "//element_symbol( elem_num(j) )//"!"
        END IF
      END DO
    END DO

    ! convert to element symbols
    elem_sym(:) = element_symbol( elem_num(:) )

    ! ------------------------------------------------------------------------------------
    ! OPEN FILE
    ! ------------------------------------------------------------------------------------
    CALL open_outfile( filename , funit , stat )
    IF(stat/=0) RETURN

    ! ------------------------------------------------------------------------------------
    ! FORMATTED PRINT
    ! ------------------------------------------------------------------------------------
    ffrmt             = 'F14.8'

    WRITE(funit,'(A)',iostat=stat) TRIM(geom%comment)
    frmt = '(1X,A)'                                   ! 2. line format
    WRITE(funit,frmt,iostat=stat) '  1.0'
    frmt = '(1X,3F12.6)'                              ! 3.-5. line format
    WRITE(funit,frmt,iostat=stat) geom%box(:,1)
    WRITE(funit,frmt,iostat=stat) geom%box(:,2)
    WRITE(funit,frmt,iostat=stat) geom%box(:,3)
    WRITE(frmt,'("(1X,",I3.3,"A6)")') atm_sorts       ! 6. line format (1X,<?>A6)
    WRITE(funit,frmt,iostat=stat) elem_sym(:atm_sorts)
    WRITE(frmt,'("(1X,",I3.3,"I6)")') atm_sorts       ! 7. line format (1X,<?>I6)
    WRITE(funit,frmt,iostat=stat) nat_elem(:atm_sorts)
    frmt = '(A)'                                      ! 8.(-9.) line format
    IF( geom%lc ) THEN
      WRITE(funit,'(A)',iostat=stat) "Silent"
    END IF
    IF( geom%sys == 0 ) THEN
      WRITE(funit,'(A)',iostat=stat) "Curse"
    ELSE
      WRITE(funit,'(A)',iostat=stat) "Drinks"
    END IF
    ! ------------------------------------------------------------------------------------
    IF( geom%lc ) THEN
      frmt = '(1X,3'//TRIM(ffrmt)//',2X,3L2,3X,"! ",A2,I4)'   ! coordinate format with constraints
      DO i = 1,geom%nat
        WRITE(funit,frmt,iostat=stat) geom%atm(i)%r , geom%atm(i)%c , &
          geom%atm(i)%c , geom%atm(i)%c , element_symbol( geom%atm(i)%e ) , i
      END DO
    ELSE
      frmt = '(1X,3'//TRIM(ffrmt)//',11X,"! ",A2,I4)'         ! coordinate format
      DO i = 1,geom%nat
        WRITE(funit,frmt,iostat=stat) geom%atm(i)%r , element_symbol( geom%atm(i)%e ) , i
      END DO
    END IF
    ! ------------------------------------------------------------------------------------
    IF( geom%lv ) THEN
      WRITE(funit,*,iostat=stat) ""                           ! empty line
      frmt = '(1X,3'//TRIM(ffrmt)//',11X,"! ",A2,I4)'         ! coordinate format
      DO i = 1,geom%nat
        WRITE(funit,frmt,iostat=stat) geom%atm(i)%v , element_symbol( geom%atm(i)%e ) , i
      END DO
    END IF
    stat = 0
    WRITE(stdout,'(A)') "Periodic geometry succesfully written to '"//TRIM(filename)//"' [vasp write]."
  END SUBROUTINE vasp_write


  SUBROUTINE displ_read( filename , nat , displ , constr , stat )
    CHARACTER(LEN=*) , INTENT(IN)                :: filename
    INTEGER          , INTENT(IN)                :: nat
    REAL(dp)         , INTENT(OUT)               :: displ
    LOGICAL          , INTENT(OUT) , ALLOCATABLE :: constr(:,:)
    INTEGER          , INTENT(OUT)               :: stat
    LOGICAL :: setdispl
    INTEGER ::  i , funit
    REAL(dp):: tmpdispl(3)

    ! open file
    CALL open_infile( filename , funit , stat )
    IF(stat/= 0) RETURN


    setdispl = .TRUE.
    IF( ALLOCATED( constr ) ) DEALLOCATE( constr )
    ALLOCATE( constr(3,nat) )
    constr = .FALSE.
    DO i = 1,nat
      READ(funit,*,iostat=stat) tmpdispl(:)
      IF( stat /= 0 ) THEN
        WRITE(stderr,'(A)') "[displ read] ERROR reading DISPLACECAR."
        RETURN
      ENDIF
      IF( tmpdispl(1) /= 0.0D0 .OR.   &
          tmpdispl(2) /= 0.0D0 .OR.   &
          tmpdispl(3) /= 0.0D0    )   THEN
        constr(:,i) = .TRUE.
        IF( setdispl ) THEN
          IF( tmpdispl(1) /= 0.0D0 ) THEN
            displ = tmpdispl(1)
            setdispl = .FALSE.
          ELSEIF( tmpdispl(2) /= 0.0D0 ) THEN
            displ = tmpdispl(2)
            setdispl = .FALSE.
          ELSEIF( tmpdispl(3) /= 0.0D0 ) THEN
            displ = tmpdispl(3)
            setdispl = .FALSE.
          ENDIF
        ENDIF
      ENDIF
    ENDDO
  END SUBROUTINE displ_read


  SUBROUTINE displ_write( filename , nat , displ , constr )
    CHARACTER(LEN=*) , INTENT(IN)  :: filename
    INTEGER , INTENT(IN)  :: nat
    REAL(dp), INTENT(IN)  :: displ
    LOGICAL , INTENT(IN)  :: constr(3,nat)
    LOGICAL :: iconstr(3,nat)
    INTEGER :: funit , i , stat

    ! open outfile
    CALL open_outfile( filename , funit , stat )

    DO i = 1,nat
      IF( constr(1,i) .OR. constr(2,i) .OR. constr(3,i) ) THEN
        WRITE(funit,'(2X,3F7.3)') displ , displ , displ
      ELSE
        WRITE(funit,'(1X,3F7.3)') 0.0 , 0.0 , 0.0
      ENDIF
    ENDDO
  END SUBROUTINE displ_write


  SUBROUTINE xyz_read( filename , atm , stat , comment )
    USE types , ONLY: atomic_number
    CHARACTER(LEN=*) , INTENT(IN)                           :: filename
    TYPE(atom)       , INTENT(OUT) , ALLOCATABLE            :: atm(:)
    INTEGER          , INTENT(OUT)                          :: stat
    CHARACTER(LEN=:) , INTENT(OUT) , ALLOCATABLE , OPTIONAL :: comment

    INTEGER              :: funit , i , nat
    CHARACTER(LEN=2)     :: elem_sym
    CHARACTER(LEN=lstr_) :: icomment

    CALL open_infile( filename , funit , stat )
    IF(stat/= 0) GOTO 73

    ! read number of atoms
    READ(funit,*,iostat=stat) nat
    IF(stat/=0) THEN; WRITE(stderr,'(I5,1X,A)') stat , "[xyz read] ERROR reading number of atoms!"; GOTO 73; ENDIF

    ! read comment
    READ(funit,'(A)',iostat=stat) icomment
    IF(stat/=0) THEN; WRITE(stderr,'(I5,1X,A)') stat , "[xyz read] ERROR reading comment!"; GOTO 73; ENDIF
    IF( PRESENT(comment) ) comment = TRIM(icomment)

    ! allocate
    IF( ALLOCATED( atm ))   DEALLOCATE( atm )
    ALLOCATE( atm(nat) )

    ! read atoms
    DO i=1,nat
      READ(funit,*,iostat=stat) elem_sym , atm(i)%r
      IF(stat/=0) THEN; WRITE(stderr,'(I5,1X,A,I4)') stat , "[xyz read] ERROR reading atom", i ,"!"; GOTO 73; ENDIF
      atm(i)%e = atomic_number( elem_sym )
    END DO

    ! clean up
    CLOSE(funit)
    WRITE(stdout,'(A)') "Data from '"//TRIM(filename)//"' read in [xyz read]."
    stat = 0
    RETURN

    ! exception
73  CLOSE(funit)
    WRITE(stdout,'(A)') "No data was read from '"//TRIM(filename)//"' [xyz read]."
    IF( ALLOCATED( atm ))   DEALLOCATE( atm )

  END SUBROUTINE xyz_read


  SUBROUTINE xyz_write( filename , atm , stat , comment )
    USE types, ONLY: element_symbol
    CHARACTER(LEN=*)     , INTENT(IN)    :: filename
    TYPE(atom)           , INTENT(IN) , ALLOCATABLE   :: atm(:)
    INTEGER              , INTENT(OUT)   :: stat
    CHARACTER(LEN=*)     , INTENT(IN) , OPTIONAL :: comment
    CHARACTER(LEN=:)     , ALLOCATABLE   :: icomment
    CHARACTER(LEN=sstr_) :: tmp

    INTEGER              :: funit , i , nat

    nat = SIZE(atm)

    IF( PRESENT(comment) ) THEN
      icomment = comment
    ELSE
      icomment = comment_
    ENDIF

    ! open file
    CALL open_outfile( filename , funit , stat )
    IF(stat/=0) RETURN

    WRITE(tmp,'(I8)') nat
    WRITE(funit,'(A)') TRIM(ADJUSTL(tmp))
    WRITE(funit,'(A)') TRIM(icomment)

    DO i = 1,nat
      WRITE(funit,'(A2,1X,3F21.16)') element_symbol(atm(i)%e) , atm(i)%r
    ENDDO

    CLOSE(funit)

    WRITE(stdout,'(A)') "[xyz write] Cartesian geometry succesfully written to '"//TRIM(filename)//"'."

  END SUBROUTINE xyz_write


  SUBROUTINE xsf_write( filename , geom , stat )
    !!! INPUT
    CHARACTER(LEN=*) , INTENT(IN)             :: filename
    TYPE(pbc)        , INTENT(IN)             :: geom
    INTEGER          , INTENT(OUT)            :: stat

    CHARACTER(LEN=str_)  :: ffrmt , frmt
    INTEGER              :: funit
    INTEGER              :: i

    ! set precision
    ffrmt             = 'F21.16'
    ! ------------------------------------------------------------------------------------
    ! convert to cartesian coordinates
    ! ------------------------------------------------------------------------------------
    ! TODO
    IF( geom%sys /= 0 ) THEN
      WRITE(stderr,'(A)') "[xsf write] ERROR: refuse to write anything but cartesian coordinates."
      RETURN
    ENDIF

    ! ------------------------------------------------------------------------------------
    ! OPEN FILE
    ! ------------------------------------------------------------------------------------
    CALL open_outfile( filename , funit , stat )
    IF(stat/=0) RETURN

    ! ------------------------------------------------------------------------------------
    ! write XSF file
    ! ------------------------------------------------------------------------------------
    WRITE(funit,'("# ",A)',iostat=stat) TRIM(geom%comment)
    frmt = '(1X,3'//TRIM(ffrmt)//')'
    WRITE(funit,'(A)',iostat=stat) 'CRYSTAL'
    WRITE(funit,'(A)',iostat=stat) 'PRIMVEC'
    WRITE(funit,frmt,iostat=stat) geom%box(:,1)
    WRITE(funit,frmt,iostat=stat) geom%box(:,2)
    WRITE(funit,frmt,iostat=stat) geom%box(:,3)
    !WRITE(funit,'(A)',iostat=stat) 'CONVVEC'
    !WRITE(funit,frmt,iostat=stat) geom%box(:,1)
    !WRITE(funit,frmt,iostat=stat) geom%box(:,2)
    !WRITE(funit,frmt,iostat=stat) geom%box(:,3)
    WRITE(funit,'(A)',iostat=stat) 'PRIMCOORD'
    WRITE(funit,'(2I4)',iostat=stat) geom%nat , 1
    IF( geom%lv ) THEN
      frmt = '(1X,I3,3X,3'//TRIM(ffrmt)//',3X,3'//TRIM(ffrmt)//')'
      DO i = 1,geom%nat
        WRITE(funit,frmt) geom%atm(i)%e , geom%atm(i)%r , geom%atm(i)%v
      ENDDO
    ELSE
      frmt = '(1X,I3,3X,3'//TRIM(ffrmt)//')'
      DO i = 1,geom%nat
        WRITE(funit,frmt) geom%atm(i)%e , geom%atm(i)%r
      ENDDO
    ENDIF
    WRITE(stdout,'(A)') "Periodic geometry succesfully written to '"//TRIM(filename)//"' (XCrysden XSF)."

    CLOSE(funit)
  END SUBROUTINE xsf_write


  FUNCTION tag_file( filename , tag , ext , dig , stderr ) RESULT( fname )

    CHARACTER(LEN=*)   , INTENT(IN)  :: filename , tag , ext
    INTEGER , OPTIONAL , INTENT(IN)  :: dig , stderr
    CHARACTER(LEN=:)   , ALLOCATABLE :: fname , frmt
    INTEGER :: ec , c , i , j , k , ste , stat
    LOGICAL :: lext
    ec  = 2       ; IF( PRESENT( dig    )) ec  = dig
    ste = stderr_ ; IF( PRESENT( stderr )) ste = stderr

    ! format
    frmt = '(I??.??)'
    WRITE(frmt(3:4),'(I2.2)') ec
    WRITE(frmt(6:7),'(I2.2)') ec


    fname = TRIM(filename)

    i = LEN(TRIM(fname))
    j = LEN(TRIM(ext))
    k = LEN(TRIM(tag))

    ! cut extension
    lext=.FALSE.
    IF( fname(i-j+1:i) == ext ) THEN
      fname = fname(1:i-j)
      i = LEN(TRIM(fname))
      lext=.TRUE.
    ENDIF

    ! enumerated output geometries starting with 2 and two digits, start overwriting after 100
    c = 1
    IF( fname(i-j+1:i) == tag .OR. fname(i-j-ec+1:i-ec) == tag ) THEN
      ! read counter
      IF( IACHAR(fname(i:i)) >= 48 .AND. IACHAR(fname(i:i)) < 58 ) THEN
        READ(fname(i-ec+1:i),frmt,iostat=stat) c
        IF( stat /= 0 ) THEN
          WRITE(ste,'(A)') "[rot] WARNING, messy filename: '"//fname//"'! Will count from 10 now."
          c = 9
        ENDIF
        IF( c == (10**ec)-1 ) c = -1
      ELSE ! append space for enumeration
        i=i+ec
        DEALLOCATE( fname )
        ALLOCATE( CHARACTER(LEN=i) :: fname )
        READ(filename,*,iostat=stat) fname
      ENDIF

      ! update counter
      WRITE(fname(i-ec+1:i),frmt) c+1

    ELSE
      fname = fname//tag
    ENDIF
    
    IF( lext ) fname = fname//ext

  END FUNCTION tag_file


  FUNCTION random_filename( length ) RESULT( name )
    ! ------------------------------------------------------------------------------------
    ! CREATE RANDOM FILENAME (lowercase characters)
    ! ------------------------------------------------------------------------------------
    INTEGER , INTENT(IN)  :: length
    CHARACTER(LEN=length) :: name
    REAL(dp) :: rnum(length)
    INTEGER  :: i

    CALL rnginit()
    CALL RANDOM_NUMBER( rnum )

    DO i = 1,length
      name(i:i) = ACHAR( 97 + FLOOR(rnum(i)*26) )
    ENDDO

  END FUNCTION random_filename


  FUNCTION file( filename )
    CHARACTER(LEN=*) , INTENT(IN)   :: filename
    CHARACTER(LEN=:) , ALLOCATABLE  :: file
    INTEGER  :: i

    DO i = LEN(filename),1,-1
      IF( filename(i:i) == '/' ) THEN
        EXIT
      ENDIF
    ENDDO

    file = filename(i+1:)

  END FUNCTION file


  FUNCTION path( filename )
    CHARACTER(LEN=*) , INTENT(IN)   :: filename
    CHARACTER(LEN=:) , ALLOCATABLE  :: path
    INTEGER  :: i

    DO i = LEN(filename),1,-1
      IF( filename(i:i) == '/' ) THEN
        EXIT
      ENDIF
    ENDDO

    path = filename(:i)

  END FUNCTION path


END MODULE file_io
